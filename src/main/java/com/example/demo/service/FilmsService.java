package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.Film;

public interface FilmsService {
	Film addFilms (Film film);
//	void updateFilms(Films film);
	void deleteUserbyId(int filmcode);
	List<Film> getAllFilms(Boolean is_Tayang);

	List<Film> getAllFilms();

}

package com.example.demo.Quartz;

import com.example.demo.serviceImpl.FilmsServiceImpl;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class LoadJobScheduler extends QuartzJobBean {
    FilmsServiceImpl filmService;
    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        filmService.getAllFilms();
    }
}

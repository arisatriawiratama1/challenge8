package com.example.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.Seat;

public interface SeatRepository extends JpaRepository<Seat, Integer>{

}
